#!/usr/bin/env python3

from distutils.core import setup

setup(name        = 'give-me-a-name',
      version     = 'give-me-a-version',
      scripts     = ['src/three.py', 'src/four.py'],
      py_modules  = ['mymodule'],
      package_dir = {'' : 'src'},
      )
