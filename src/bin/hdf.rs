use std::error::Error;
use ndarray::s;

fn main() -> Result<(), Box<dyn Error>> {

    let data = vec![Table { a: 1, b: 2 },
                    Table { a: 3, b: 4 },
    ];

    hdf5::File::create("test.h5")?
        .create_group("group")?
        .new_dataset_builder()
        .with_data(&data)
        .create("table")?;

    println!("Managed to write and read an HDF5 table.");

    let recovered_data = hdf5::File::open("test.h5")?
        .dataset("group/table")?
        .read_slice_1d::<Table, _>(s![..])?;

    assert_eq!(data[0], recovered_data[0]);
    assert_eq!(data[1], recovered_data[1]);

    Ok(())
}

#[derive(hdf5::H5Type, Clone, PartialEq, Debug)]
#[repr(C)]
pub struct Table {
    a: u16,
    b: u16,
}
