#!/usr/bin/env python3

print('This is the THIRD executable ... about to import numpy')

import numpy

print(f'Got numpy: {numpy}')

print('This is the THIRD executable about to import our own module')

import mymodule

print(mymodule.thing)
