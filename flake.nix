{
  description = "Nixified Rust project";

  inputs = {
    nixpkgs         .url = "github:nixos/nixpkgs/nixos-unstable";
    utils           .url = "github:numtide/flake-utils";
    rust-overlay = { url = "github:oxalica/rust-overlay"; inputs.nixpkgs    .follows = "nixpkgs";
                                                          inputs.flake-utils.follows = "utils"; };
    crate2nix    = { url = "github:kolloch/crate2nix";     flake = false; };
    flake-compat = { url = "github:edolstra/flake-compat"; flake = false; };
  };

  outputs = { self, nixpkgs, utils, rust-overlay, crate2nix, ... }:
    let
      name = "nixified-rust-project";
    in

    # Option 1: try to support each default system
    # utils.lib.eachDefaultSystem # NB Some packages in nixpkgs are not supported on some systems

    # Option 2: try to support selected systems
    utils.lib.eachSystem ["x86_64-linux" "aarch64-darwin" "x86_64-darwin"] # ["x86_64-linux" "i686-linux" "aarch64-linux" "x86_64-darwin"]
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              # ===== Specification of the rust toolchain to be used ====================
              rust-overlay.overlay (final: prev:
                let
                  # If you have a rust-toolchain file for rustup, choose `rustup =
                  # rust-tcfile` further down to get the customized toolchain
                  # derivation.
                  rust-tcfile  = final.rust-bin.fromRustupToolchainFile ./rust-toolchain;
                  rust-latest  = final.rust-bin.stable .latest      ;
                  rust-beta    = final.rust-bin.beta   .latest      ;
                  rust-nightly = final.rust-bin.nightly."2022-02-07";
                  rust-stable  = final.rust-bin.stable ."1.58.1"    ; # nix flake lock --update-input rust-overlay
                  rust-analyzer-preview-on = date:
                    final.rust-bin.nightly.${date}.default.override
                      { extensions = [ "rust-analyzer-preview" ]; };
                in
                  rec {
                    # The version of the Rust system to be used in buldiInputs. Choose between
                    # tcfile/latest/beta/nightly/stable (see above) on the next line
                    rustup = rust-stable;

                    rustc = rustup.default;
                    cargo = rustup.default;
                    rust-analyzer-preview = rust-analyzer-preview-on "2022-02-07";
                  })
            ];
          };

          # ----- Conditional inclusion ----------------------------------------------------
          nothing = pkgs.coreutils;
          linux      = drvn: if pkgs.stdenv.isLinux  then drvn else nothing;
          darwin     = drvn: if pkgs.stdenv.isDarwin then drvn else nothing;
          linuxList  = list: if pkgs.stdenv.isLinux  then list else [];
          darwinList = list: if pkgs.stdenv.isDarwin then list else [];
          darwinStr  = str:  if pkgs.stdenv.isDarwin then str  else "";

          # ----- Darwin-specific ----------------------------------------------------------
          darwin-frameworks = pkgs.darwin.apple_sdk.frameworks;

          # ----- Python modules -----------------------------------------------------------
          my-python-packages = pypkgs: [
            pypkgs.numpy
          ];

          inherit (import "${crate2nix}/tools.nix" { inherit pkgs; }) generatedCargoNix;

          # Create the cargo2nix project
          project = import (generatedCargoNix {
            inherit name;
            src = ./.;
          })
            {
              inherit pkgs;
              # See "Handling external dependencies" on
              # https://ryantm.github.io/nixpkgs/languages-frameworks/rust/#rust
              defaultCrateOverrides = pkgs.defaultCrateOverrides // {

                rust-darwin-flake = old-attributes: {
                  buildInputs = [ (darwin darwin-frameworks.AppKit) ];
                };

                hdf5-sys = old-attributes: {
                  buildInputs = [ pkgs.hdf5 ];
                  HDF5_DIR = pkgs.symlinkJoin { name = "hdf5"; paths = [ pkgs.hdf5 pkgs.hdf5.dev ]; };
                };

              };
            };

          # non-Rust dependencies
          buildInputs = [ pkgs.hdf5 ];
          nativeBuildInputs = [ pkgs.rustc pkgs.cargo ];
        in
        rec {
          packages."${name}-rust" = project.rootCrate.build;

          packages."${name}-python" = pkgs.python3Packages.buildPythonApplication {
            pname = "${name}-python";
            version = "TODO-version";
            src = ./.;
            propagatedBuildInputs = [ (my-python-packages pkgs.python3Packages) ];
          };

          packages."${name}-env" = pkgs.buildEnv {
            name = "${name}-env";
            paths = [
              packages."${name}-rust"
              packages."${name}-python"
            ];
          };

          # ========== nix build =========================================================
          defaultPackage = packages."${name}-env";

          # ========== nix run ============================================================
          defaultApp = apps.one;

          apps.${name} = utils.lib.mkApp {
            inherit name;
            drv = packages.${name};
          };

          apps.one   = utils.lib.mkApp { drv = packages."${name}-env"; name = "one"; };
          apps.two   = utils.lib.mkApp { drv = packages."${name}-env"; name = "two"; };
          apps.hdf   = utils.lib.mkApp { drv = packages."${name}-env"; name = "hdf"; };
          apps.ogl   = utils.lib.mkApp { drv = packages."${name}-env"; name = "ogl"; };
          apps.three = utils.lib.mkApp { drv = packages."${name}-env"; name = "three.py"; };
          apps.four  = utils.lib.mkApp { drv = packages."${name}-env"; name = "four.py"; };

          # ========== nix develop ========================================================
          devShell = pkgs.mkShell {
            inputsFrom = builtins.attrValues self.packages.${system};
            buildInputs = buildInputs ++ [
              # Tools you need for development go here.
              pkgs.rust-analyzer-preview
              #pkgs.rustup.rls pkgs.rustup.rust-analysis
            ];
            RUST_SRC_PATH = "${pkgs.rustup.rust-src}/lib/rustlib/src/rust/library";
            HDF5_DIR = pkgs.symlinkJoin { name = "hdf5"; paths = [ pkgs.hdf5 pkgs.hdf5.dev ]; };
          };
        }
      );
}
